<?php

require_once __DIR__ . '/autoloader.php';

// Processing arguments
if ($argc !== 3) {
    echo 'Required arguments are missing. Usage: "php user_text_util.php <comma|semicolon> <countAverageLineCount|replaceDates>".' . PHP_EOL;
    exit(1);
}

if ($argv[1] === 'comma') {
    $separator = ',';
} else if ($argv[1] === 'semicolon') {
    $separator = ';';
} else {
    echo "Invalid separator value \"{$argv[1]}\" given. Only \"comma\" and \"semicolon\" are supported." . PHP_EOL;
    exit(1);
}

if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $argv[2])) {
    echo "Invalid action value given \"{$argv[2]}\"." . PHP_EOL;
    exit(1);
}

// Preparing
$workingDirectory = getcwd();
if ($workingDirectory === false) {
    echo 'Something went wrong... Please try again or contact the author.' . PHP_EOL;
    exit(1);
}

$usersFilePath = $workingDirectory . '/people.csv';
if (!is_file($usersFilePath)) {
    echo "Unable to find users file at path \"{$usersFilePath}\"." . PHP_EOL;
    exit(1);
}

$processorName = '\\App\\Processor\\' . ucfirst($argv[2]) . 'Processor';
if (!class_exists($processorName)) {
    echo "Invalid action value given \"{$argv[2]}\"." . PHP_EOL;
    exit(1);
}

// Let's do it!
/** @var \App\Processor\IProcessor $processor */
$processor = new $processorName($workingDirectory);
if (!($processor instanceof \App\Processor\IProcessor)) {
    echo "Invalid action value given \"{$argv[2]}\"." . PHP_EOL;
    exit(1);
}

$f = fopen($usersFilePath, 'rb');
$absoluteLine = 0;

while ($line = fgets($f)) {
    $absoluteLine++;
    $row = str_getcsv($line, $separator);

    if (count($row) !== 2) {
        fclose($f);
        echo "Incorrect data format at line #{$absoluteLine} in {$usersFilePath}" . PHP_EOL;
        exit(1);
    }

    try {
        $processor->run((int) $row[0], $row[1]);
    } catch (\Exception $e) {
        fclose($f);
        echo $e->getMessage() . PHP_EOL;
        exit(1);
    }
}

fclose($f);
