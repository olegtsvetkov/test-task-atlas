<?php

namespace App\Processor;


interface IProcessor
{
    /**
     * Process user related action
     *
     * @param int $userId
     * @param string $userName
     */
    public function run($userId, $userName);
}
