<?php

namespace App\Processor;

use App\Helpers\FileHelper;

/**
 * CountAverageLineCountProcessor processor takes users texts form ./text/ directory and count an average amount of
 * lines per file for each user.
 *
 * @package App\Processor
 */
class CountAverageLineCountProcessor extends AbstractProcessor
{
    /**
     * @inheritdoc
     */
    public function run($userId, $userName)
    {
        $rowCount = 0;
        $files = 0;

        foreach (new \GlobIterator($this->textsDirectory . "/{$userId}-*.txt") as $file) {
            /** @var \SplFileInfo $file */
            $rowCount += FileHelper::getLinesCount($file->getRealPath());
            $files++;
        }

        if ($files === 0) {
            $this->println("$userName has no text files.");
        } else {
            $this->println(sprintf('%s: %.2f lines avg in %d files.', $userName, $rowCount / $files, $files));
        }
    }
}
