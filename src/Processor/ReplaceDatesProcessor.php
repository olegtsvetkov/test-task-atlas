<?php

namespace App\Processor;

/**
 * ReplaceDatesProcessor takes users text from ./text/ directory and replace the dates format (dd/mm/yy -> mm-dd-yyyy).
 * Result is being stored in ./output_texts/ directory.
 *
 * @package App\Processor
 */
class ReplaceDatesProcessor extends AbstractProcessor
{
    /**
     * @var string
     */
    protected $outputDirectory;

    /**
     * @inheritdoc
     */
    public function __construct($workingDirectory)
    {
        parent::__construct($workingDirectory);

        $this->outputDirectory = $workingDirectory . '/output_texts';
    }

    /**
     * @inheritdoc
     */
    public function run($userId, $userName)
    {
        // Creating the output folder
        if (!is_dir($this->outputDirectory) && !@mkdir($this->outputDirectory) && !is_dir($this->outputDirectory)) {
            throw new \RuntimeException('Unable to create an output directory.');
        }

        $replacesCount = 0;

        // Iterating over original texts
        foreach (new \GlobIterator($this->textsDirectory . "/{$userId}-*.txt") as $file) {
            /** @var \SplFileInfo $file */
            $originalFile = fopen($file->getRealPath(), 'rb');
            $outputFile = fopen($this->outputDirectory . '/' . $file->getFilename(), 'wb+');

            while ($line = fgets($originalFile)) {
                if (preg_match_all('/\b(\d{2}\/\d{2}\/\d{2})\b/', $line, $matches)) {
                    foreach ($matches[1] as $match) {
                        // Convert to DateTime object and compare with an original date string to verify, that date is correct.
                        $datetime = \DateTime::createFromFormat('d/m/y', $match);
                        if ($datetime->format('d/m/y') !== $match) {
                            continue;
                        }

                        // Replacing
                        $line = str_replace($match, $datetime->format('m-d-Y'), $line);

                        $replacesCount++;
                    }
                }

                fwrite($outputFile, $line);
            }

            fclose($outputFile);
            fclose($originalFile);
        }

        // Output the result
        $this->println(sprintf('%s: %d replaces done.', $userName, $replacesCount));
    }
}
