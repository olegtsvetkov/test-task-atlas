<?php

namespace App\Processor;


abstract class AbstractProcessor implements IProcessor
{
    /**
     * @var string
     */
    protected $workingDirectory;
    /**
     * @var string
     */
    protected $textsDirectory;

    /**
     * @param string $workingDirectory
     */
    public function __construct($workingDirectory)
    {
        $this->workingDirectory = $workingDirectory;
        $this->textsDirectory = $this->workingDirectory . '/texts';
    }

    /**
     * @param string $text
     */
    protected function println($text)
    {
        echo $text . PHP_EOL;
    }
}
