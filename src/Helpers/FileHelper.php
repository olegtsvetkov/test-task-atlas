<?php

namespace App\Helpers;

class FileHelper
{
    /**
     * Memory efficient line counter.
     *
     * @param string $file path to file, in which we count lines
     * @return int amount of lines
     */
    public static function getLinesCount($file)
    {
        $f = fopen($file, 'rb');
        $lines = 0;

        while (!feof($f)) {
            $lines += substr_count(fread($f, 8192), "\n");
        }

        fclose($f);

        return $lines;
    }
}
